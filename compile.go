// Copyright 2011 The Go Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package template

import (
	"fmt"
	"io"
	"reflect"
	"bitbucket.org/tailed/template/parse"
)

var zero_c = variable{name: "zero"}

func chomp_leading_dollar(name string) string {
	if name[0] == '$' {
		return name[1:]
	} else {
		return name
	}
}

func (s *state) push_c(name string, value variable) {
	fmt.Println(chomp_leading_dollar(name) + " := " + value.name)
	s.vars = append(s.vars, variable{name, value.value})
}

// printValue writes the textual representation of the value to the output of
// the template.
func (s *state) printValue_c(n parse.Node, v variable) {
	s.at(n)
	name, ok := printableValue_c(v)
	if !ok {
		s.errorf("can't print %s of type %s", n, v.value.Type())
	}
	_, err := fmt.Fprintf(s.wr, "fmt.Fprint(w, %s)\n", name)
	if err != nil {
		s.writeError(err)
	}
}

// printableValue returns the, possibly indirected, interface value inside v that
// is best for a call to formatted printer.
func printableValue_c(v variable) (string, bool) {
	if v.value.Kind() == reflect.Ptr {
		v, _ = indirect_c(v) // fmt.Fprint handles nil.
	}
	if !v.value.IsValid() {
		return "<no value>", true
	}

	if !v.value.Type().Implements(errorType) && !v.value.Type().Implements(fmtStringerType) {
		if v.value.CanAddr() && (reflect.PtrTo(v.value.Type()).Implements(errorType) || reflect.PtrTo(v.value.Type()).Implements(fmtStringerType)) {
			v = variable{"&(" + v.name + ")", v.value.Addr()}
		} else {
			switch v.value.Kind() {
			case reflect.Chan, reflect.Func:
				return "<nil>", false
			}
		}
	}
	return v.name, true
}



func (t *Template) Compile(wr io.Writer, data interface{}) (err error) {
	defer errRecover(&err)
	value, ok := data.(reflect.Value)
	if !ok {
		value = reflect.ValueOf(data)
	}
	state := &state{
		tmpl: t,
		wr:   wr,
		vars: []variable{{"$", value}},
	}
	if t.Tree == nil || t.Root == nil {
		state.errorf("%q is an incomplete or empty template", t.Name())
	}
	fmt.Fprintf(state.wr, "func Template_%s(w io.Writer, data %s) {\n", t.name, value.Type().Name())
	state.walk_c(variable{"data", value}, t.Root)
	fmt.Fprintf(state.wr, "}\n")
	return
}

func (s *state) walk_c(dot variable, node parse.Node) {
	s.at(node)
	switch node := node.(type) {
	case *parse.ActionNode:
		// Do not pop variables so they persist until next end.
		// Also, if the action declares variables, don't print the result.
		val := s.evalPipeline_c(dot, node.Pipe, false)
		if len(node.Pipe.Decl) == 0 {
			s.printValue_c(node, val)
		}
	case *parse.IfNode:
		s.walkIfOrWith_c(parse.NodeIf, dot, node.Pipe, node.List, node.ElseList)
	case *parse.ListNode:
		for _, node := range node.Nodes {
			s.walk_c(dot, node)
		}
	case *parse.RangeNode:
		s.walkRange_c(dot, node)
	case *parse.TemplateNode:
		s.walkTemplate_c(dot, node)
	case *parse.TextNode:
		if _, err := fmt.Fprintf(s.wr, "fmt.Fprint(w, `%s`)\n", node.Text); err != nil {
			s.writeError(err)
		}
	case *parse.WithNode:
		s.walkIfOrWith_c(parse.NodeWith, dot, node.Pipe, node.List, node.ElseList)
	default:
		s.errorf("unknown node: %s", node)
	}
}

func (s *state) walkTemplate_c(dot variable, t *parse.TemplateNode) {
	s.at(t)
	tmpl := s.tmpl.tmpl[t.Name]
	if tmpl == nil {
		s.errorf("template %q not defined", t.Name)
	}
	if s.depth == maxExecDepth {
		s.errorf("exceeded maximum template depth (%v)", maxExecDepth)
	}
	// Variables declared by the pipeline persist.
	dot = s.evalPipeline_c(dot, t.Pipe, false)
	newState := *s
	newState.depth++
	newState.tmpl = tmpl
	// No dynamic scoping: template invocations inherit no variables.
	newState.vars = []variable{{"$", dot.value}}
	newState.walk_c(dot, tmpl.Root)
}


func (s *state) walkRange_c(dot variable, r *parse.RangeNode) {
	s.at(r)
	defer s.pop(s.mark())
	val, _ := indirect_c(s.evalPipeline_c(dot, r.Pipe, true))
	// mark top of stack before any variables in the body are pushed.
	mark := s.mark()
	oneIteration := func(index reflect.Value, elem variable) {
		// Set top var (lexically the second if there are two) to the element.
		if len(r.Pipe.Decl) > 0 {
			s.setVar(1, elem.value)
		}
		// Set next var (lexically the first if there are two) to the index.
		if len(r.Pipe.Decl) > 1 {
			s.setVar(2, index)
		}
		s.walk_c(elem, r.List)
		s.pop(mark)
	}
	index_name := s.vars[len(s.vars)-2].name
	elem_name := s.vars[len(s.vars)-1].name
	fmt.Fprintf(s.wr, "for %s, %s := range %s {\n", chomp_leading_dollar(index_name), chomp_leading_dollar(elem_name), val.name)
	defer (func() { fmt.Fprintf(s.wr, "}\n") })()
	switch val.value.Kind() {
	case reflect.Array, reflect.Slice:
		if val.value.Len() == 0 {
			fmt.Fprintln(s.wr, "/* Warning: No element was looped in range, so precompile was failed */")
			break
		}
		for i := 0; i < val.value.Len(); i++ {
			oneIteration(reflect.ValueOf(i), variable{elem_name, val.value.Index(i)})
			break /* Do only one iteration */
		}
		return
	case reflect.Map:
		if val.value.Len() == 0 {
			fmt.Fprintln(s.wr, "/* Warning: No element was looped in range, so precompile was failed */")
			break
		}
		for _, key := range sortKeys(val.value.MapKeys()) {
			oneIteration(key, variable{elem_name, val.value.MapIndex(key)})
			break /* Do only one iteration */
		}
		return
	case reflect.Chan:
		panic("NOT IMPLEMENTED: Chan")
		/*
		if val.value.IsNil() {
			break
		}
		i := 0
		for ; ; i++ {
			elem, ok := val.value.Recv()
			if !ok {
				break
			}
			oneIteration(reflect.ValueOf(i), elem)
		}
		if i == 0 {
			break
		}
		return
		*/
	case reflect.Invalid:
		break // An invalid value is likely a nil map, etc. and acts like an empty map.
	default:
		s.errorf("range can't iterate over %v", val)
	}
	if r.ElseList != nil {
		s.errorf("range's else is not implemented")
//		s.walk_c(dot, r.ElseList)
	}
}



// walkIfOrWith walks an 'if' or 'with' node. The two control structures
// are identical in behavior except that 'with' sets dot.
func (s *state) walkIfOrWith_c(typ parse.NodeType, dot variable, pipe *parse.PipeNode, list, elseList *parse.ListNode) {
	defer s.pop(s.mark())
	val := s.evalPipeline_c(dot, pipe, false)
	_, ok := isTrue(val.value) /* ignore truth value! i.e. output both of branches */
	if !ok {
		s.errorf("if/with can't use %v", val)
	}
	fmt.Fprintf(s.wr, "if %s {\n", val.name)
	if typ == parse.NodeWith {
		s.walk_c(val, list) /* Maybe fail because val might be nil */
	} else {
		s.walk_c(dot, list)
	}
	fmt.Fprint(s.wr, "}")
	if elseList != nil {
		fmt.Fprint(s.wr, " else {\n")
		s.walk_c(dot, elseList)
		fmt.Fprint(s.wr, "}")
	}
	fmt.Fprint(s.wr, "\n")
}


func (s *state) evalPipeline_c(dot variable, pipe *parse.PipeNode, ignoreDecl bool) (value variable) {
	if pipe == nil {
		return
	}
	s.at(pipe)
	for _, cmd := range pipe.Cmds {
		value = s.evalCommand_c(dot, cmd, value) // previous value is this one's final arg.
		// If the object has type interface{}, dig down one level to the thing inside.
		if value.value.Kind() == reflect.Interface && value.value.Type().NumMethod() == 0 { /* XXX ??? */
			value.value = reflect.ValueOf(value.value.Interface()) // lovely!
		}
	}
	for _, variable := range pipe.Decl {
		if ignoreDecl { /* range case */
			s.push(variable.Ident[0], value.value)
		} else {
			s.push_c(variable.Ident[0], value)
		}
	}
	return value
}

func (s *state) evalCommand_c(dot variable, cmd *parse.CommandNode, final variable) variable {
	firstWord := cmd.Args[0]
	switch n := firstWord.(type) {
	case *parse.FieldNode:
		return s.evalFieldNode_c(dot, n, cmd.Args, final)
	case *parse.ChainNode:
		panic("not implemented chain")
//		return s.evalChainNode(dot.value, n, cmd.Args, final)
	case *parse.IdentifierNode:
		// Must be a function.
		return s.evalFunction_c(dot, n, cmd, cmd.Args, final)
	case *parse.PipeNode:
		// Parenthesized pipeline. The arguments are all inside the pipeline; final is ignored.
		panic("not implemented pipe")
//		return s.evalPipeline(dot.value, n)
	case *parse.VariableNode:
		return s.evalVariableNode_c(dot, n, cmd.Args, final)
	}
	panic("not implemented")
	/*
	s.at(firstWord)
	s.notAFunction(cmd.Args, final)
	switch word := firstWord.(type) {
	case *parse.BoolNode:
		return reflect.ValueOf(word.True)
	case *parse.DotNode:
		return dot.value
	case *parse.NilNode:
		s.errorf("nil is not a command")
	case *parse.NumberNode:
		return s.idealConstant(word)
	case *parse.StringNode:
		return reflect.ValueOf(word.Text)
	}
	s.errorf("can't evaluate command %q", firstWord)
	panic("not reached")
	*/
}

func (s *state) evalFieldNode_c(dot variable, field *parse.FieldNode, args []parse.Node, final variable) variable {
	s.at(field)
	return s.evalFieldChain_c(dot, dot, field, field.Ident, args, final)
}

/*
func (s *state) evalChainNode(dot reflect.Value, chain *parse.ChainNode, args []parse.Node, final reflect.Value) reflect.Value {
	s.at(chain)
	if len(chain.Field) == 0 {
		s.errorf("internal error: no fields in evalChainNode")
	}
	if chain.Node.Type() == parse.NodeNil {
		s.errorf("indirection through explicit nil in %s", chain)
	}
	// (pipe).Field1.Field2 has pipe as .Node, fields as .Field. Eval the pipeline, then the fields.
	pipe := s.evalArg(dot, nil, chain.Node)
	return s.evalFieldChain(dot, pipe, chain, chain.Field, args, final)
}

*/

func (s *state) evalVariableNode_c(dot variable, vari *parse.VariableNode, args []parse.Node, final variable) variable {
	// $x.Field has $x as the first ident, Field as the second. Eval the var, then the fields.
	s.at(vari)
	value := s.varValue(vari.Ident[0])
	pair := variable{chomp_leading_dollar(vari.Ident[0]), value}
	if len(vari.Ident) == 1 {
		s.notAFunction(args, final.value)
		return pair
	}
	return s.evalFieldChain_c(dot, pair, vari, vari.Ident[1:], args, final)
}

// evalFieldChain evaluates .X.Y.Z possibly followed by arguments.
// dot is the environment in which to evaluate arguments, while
// receiver is the value being walked along the chain.
func (s *state) evalFieldChain_c(dot, receiver variable, node parse.Node, ident []string, args []parse.Node, final variable) variable {
	n := len(ident)
	for i := 0; i < n-1; i++ {
		receiver = s.evalField_c(dot, ident[i], node, nil, zero_c, receiver)
	}
	// Now if it's a method, it gets the arguments.
	return s.evalField_c(dot, ident[n-1], node, args, final, receiver)
}

func (s *state) evalFunction_c(dot variable, node *parse.IdentifierNode, cmd parse.Node, args []parse.Node, final variable) variable {
	s.at(node)
	name := node.Ident
	function, ok := findFunction(name, s.tmpl)
	if !ok {
		s.errorf("%q is not a defined function", name)
	}
	return s.evalCall_c(dot, function, cmd, name, args, final)
}

// indirect returns the item at the end of indirection, and a bool to indicate if it's nil.
func indirect_c(v variable) (rv variable, isNil bool) {
	for ; v.value.Kind() == reflect.Ptr || v.value.Kind() == reflect.Interface; v = (variable{"*(" + v.name + ")", v.value.Elem()}) {
		if v.value.IsNil() {
			return v, true
		}
	}
	return v, false
}



// evalField evaluates an expression like (.Field) or (.Field arg1 arg2).
// The 'final' argument represents the return value from the preceding
// value of the pipeline, if any.
func (s *state) evalField_c(dot variable, fieldName string, node parse.Node, args []parse.Node, final, receiver variable) variable {
	if !receiver.value.IsValid() {
		if s.tmpl.option.missingKey == mapError { // Treat invalid value as missing map key.
			s.errorf("nil data; no entry for key %q", fieldName)
		}
		return zero_c
	}
	typ := receiver.value.Type()
	receiver, isNil := indirect_c(receiver)
	// Unless it's an interface, need to get to a value of type *T to guarantee
	// we see all methods of T and *T.
	ptr := receiver.value
	if ptr.Kind() != reflect.Interface && ptr.Kind() != reflect.Ptr && ptr.CanAddr() {
		ptr = ptr.Addr()
	}
	if method := ptr.MethodByName(fieldName); method.IsValid() {
		panic("evalCall member")
		return variable{"(dummy)", s.evalCall(dot.value, method, node, fieldName, args, final.value)}
	}
	hasArgs := len(args) > 1 || final.value.IsValid()
	// It's not a method; must be a field of a struct or an element of a map.
	switch receiver.value.Kind() {
	case reflect.Struct:
		tField, ok := receiver.value.Type().FieldByName(fieldName)
		if ok {
			if isNil {
				s.errorf("nil pointer evaluating %s.%s", typ, fieldName)
			}
			field := receiver.value.FieldByIndex(tField.Index)
			if tField.PkgPath != "" { // field is unexported
				s.errorf("%s is an unexported field of struct type %s", fieldName, typ)
			}
			// If it's a function, we must call it.
			if hasArgs {
				s.errorf("%s has arguments but cannot be invoked as function", fieldName)
			}
			return variable{receiver.name + "." + fieldName, field}
		}
	case reflect.Map:
		if isNil {
			s.errorf("nil pointer evaluating %s.%s", typ, fieldName)
		}
		// If it's a map, attempt to use the field name as a key.
		nameVal := reflect.ValueOf(fieldName)
		if nameVal.Type().AssignableTo(receiver.value.Type().Key()) {
			if hasArgs {
				s.errorf("%s is not a method but has arguments", fieldName)
			}
			result := receiver.value.MapIndex(nameVal)
			if !result.IsValid() {
				switch s.tmpl.option.missingKey {
				case mapInvalid:
					// Just use the invalid value.
				case mapZeroValue:
					result = reflect.Zero(receiver.value.Type().Elem())
				case mapError:
					s.errorf("map has no entry for key %q", fieldName)
				}
			}
			return variable{receiver.name + "[\"" + fieldName + "\"]", result}
		}
	}
	s.errorf("can't evaluate field %s in type %s", fieldName, typ)
	panic("not reached")
}

// evalCall executes a function or method call. If it's a method, fun already has the receiver bound, so
// it looks just like a function call. The arg list, if non-nil, includes (in the manner of the shell), arg[0]
// as the function itself.
func (s *state) evalCall_c(dot variable, fun reflect.Value, node parse.Node, name string, args []parse.Node, final variable) variable {
	if args != nil {
		args = args[1:] // Zeroth arg is function name/node; not passed to function.
	}
	typ := fun.Type()
	numIn := len(args)
	if final.value.IsValid() {
		numIn++
	}
	numFixed := len(args)
	if typ.IsVariadic() {
		numFixed = typ.NumIn() - 1 // last arg is the variadic one.
		if numIn < numFixed {
			s.errorf("wrong number of args for %s: want at least %d got %d", name, typ.NumIn()-1, len(args))
		}
	} else if numIn != typ.NumIn() {
		s.errorf("wrong number of args for %s: want %d got %d", name, typ.NumIn(), len(args))
	}
	if !goodFunc(typ) {
		// TODO: This could still be a confusing error; maybe goodFunc should provide info.
		s.errorf("can't call method/function %q with %d results", name, typ.NumOut())
	}
	// Build the arg list.
	argv := make([]variable, numIn)
	// Args must be evaluated. Fixed args first.
	i := 0
	for ; i < numFixed && i < len(args); i++ {
		argv[i] = s.evalArg_c(dot, typ.In(i), args[i])
	}
	// Now the ... args.
	if typ.IsVariadic() {
		argType := typ.In(typ.NumIn() - 1).Elem() // Argument is a slice.
		for ; i < len(args); i++ {
			argv[i] = s.evalArg_c(dot, argType, args[i])
		}
	}
	// Add final value if necessary.
	if final.value.IsValid() {
		t := typ.In(typ.NumIn() - 1)
		if typ.IsVariadic() {
			if numIn-1 < numFixed {
				// The added final argument corresponds to a fixed parameter of the function.
				// Validate against the type of the actual parameter.
				t = typ.In(numIn - 1)
			} else {
				// The added final argument corresponds to the variadic part.
				// Validate against the type of the elements of the variadic slice.
				t = t.Elem()
			}
		}
		argv[i] = s.validateType_c(final, t)
	}
	argv_value := make([]reflect.Value, numIn)
	argv_names := ""
	for i, v := range argv {
		argv_value[i] = v.value
		if i > 0 {
			argv_names += ", "
		}
		argv_names += v.name
	}
	result := fun.Call(argv_value)
	// If we have an error that is not nil, stop execution and return that error to the caller.
	if len(result) == 2 && !result[1].IsNil() {
		s.at(node)
		s.errorf("error calling %s: %s", name, result[1].Interface().(error))
	}
	v := result[0]
	if v.Type() == reflectValueType {
		v = v.Interface().(reflect.Value)
	}
	return variable{name + "(" + argv_names + ")", v}
}


// validateType guarantees that the value is valid and assignable to the type.
func (s *state) validateType_c(value variable, typ reflect.Type) variable {
	return variable{value.name, s.validateType(value.value, typ)}
}

func (s *state) evalArg_c(dot variable, typ reflect.Type, n parse.Node) variable {
	s.at(n)
	switch arg := n.(type) {
	case *parse.DotNode:
		return s.validateType_c(dot, typ)
	case *parse.NilNode:
		if canBeNil(typ) {
			return variable{"zero", reflect.Zero(typ)} /* TODO */
		}
		s.errorf("cannot assign nil to %s", typ)
	case *parse.FieldNode:
		return s.validateType_c(s.evalFieldNode_c(dot, arg, []parse.Node{n}, zero_c), typ)
	case *parse.VariableNode:
		return s.validateType_c(s.evalVariableNode_c(dot, arg, nil, zero_c), typ)
	case *parse.PipeNode:
		panic("not implemented")
//		return s.validateType(s.evalPipeline(dot, arg), typ)
	case *parse.IdentifierNode:
		panic("not implemented")
//		return s.validateType(s.evalFunction(dot, arg, arg, nil, zero), typ)
	case *parse.ChainNode:
		panic("not implemented")
//		return s.validateType(s.evalChainNode(dot, arg, nil, zero), typ)
	}
	switch typ.Kind() {
	case reflect.Bool:
		return literalWrapper(s.evalBool(typ, n))
	case reflect.Complex64, reflect.Complex128:
		return literalWrapper(s.evalComplex(typ, n))
	case reflect.Float32, reflect.Float64:
		return literalWrapper(s.evalFloat(typ, n))
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		return literalWrapper(s.evalInteger(typ, n))
	case reflect.Interface:
		if typ.NumMethod() == 0 {
			return s.evalEmptyInterface_c(dot, n)
		}
	case reflect.Struct:
		if typ == reflectValueType {
			res := s.evalEmptyInterface_c(dot, n)
			return variable{res.name, reflect.ValueOf(res.value)}
		}
	case reflect.String:
		return literalWrapper_string(s.evalString(typ, n))
	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64, reflect.Uintptr:
		return literalWrapper(s.evalUnsignedInteger(typ, n))
	}
	s.errorf("can't handle %s for arg of type %s", n, typ)
	panic("not reached")
}

func literalWrapper(v reflect.Value) variable {
	return variable{fmt.Sprint(v), v}
}
func literalWrapper_string(v reflect.Value) variable {
	return variable{fmt.Sprintf("`%s`", v), v}
}

func (s *state) evalEmptyInterface_c(dot variable, n parse.Node) variable {
	s.at(n)
	switch n := n.(type) {
	case *parse.BoolNode:
		return literalWrapper(reflect.ValueOf(n.True))
	case *parse.DotNode:
		return dot
	case *parse.FieldNode:
		return s.evalFieldNode_c(dot, n, nil, zero_c)
	case *parse.IdentifierNode:
		return s.evalFunction_c(dot, n, n, nil, zero_c)
	case *parse.NilNode:
		// NilNode is handled in evalArg, the only place that calls here.
		s.errorf("evalEmptyInterface: nil (can't happen)")
	case *parse.NumberNode:
		return literalWrapper(s.idealConstant(n))
	case *parse.StringNode:
		return literalWrapper_string(reflect.ValueOf(n.Text))
	case *parse.VariableNode:
		return s.evalVariableNode_c(dot, n, nil, zero_c)
	case *parse.PipeNode:
		return s.evalPipeline_c(dot, n, false)
	}
	s.errorf("can't handle assignment of %s to empty interface argument", n)
	panic("not reached")
}


