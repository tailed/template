# README #

Precompiling text/template.

### Usage ###

Change
* import "text/template"
into
* import "bitbucket.org/tailed/template"
Then, simply run the code.  It will produce a "template\_filename\_linenum.go" file,
which contains a precompiled template code.

Instead, you may use "PreCompile(w io.Writer)" or "PreCompileTemplate(w io.Writer, name string)" function.

### Memo ###

* Currently, any type information is not used in precompile.go.  As a consequence, it is impossible to detect a pointer.
