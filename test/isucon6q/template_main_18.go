package main
import (
	"io"
	"fmt"
	"text/template"
)
func PrecompiledTmpl_main_18(w io.Writer, data *Data) error {
{
data := data
_ = data
w.Write([]byte(`<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html" charset="utf-8">
    <title>Isuda</title>
    <link rel="shortcut icon" href="`))
fmt.Fprint(w, url_for(`favicon.ico`))
w.Write([]byte(`" type="image/vnd.microsoft.icon" />
    <link rel="stylesheet" href="`))
fmt.Fprint(w, url_for(`/css/bootstrap.min.css`))
w.Write([]byte(`">
    <link rel="stylesheet" href="`))
fmt.Fprint(w, url_for(`/css/bootstrap-responsive.min.css`))
w.Write([]byte(`">
    <link rel="stylesheet" href="`))
fmt.Fprint(w, url_for(`/css/main.css`))
w.Write([]byte(`">
  </head>
  <body>

    <div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <a class="brand" href="`))
fmt.Fprint(w, url_for(`/`))
w.Write([]byte(`">Isuda</a>
          <div class="nav-collapse">
            <ul class="nav">
              <li><a href="`))
fmt.Fprint(w, url_for(`/`))
w.Write([]byte(`">Home</a></li>
              <li><a href="`))
fmt.Fprint(w, url_for(`/login`))
w.Write([]byte(`">Login</a></li>
              <li><a href="`))
fmt.Fprint(w, url_for(`/register`))
w.Write([]byte(`">Register</a></li>
            </ul>
          </div> <!--/.nav-collapse -->
        </div>
      </div>
    </div>

    <div class="container">
      <p>Hello <span class="isu-account-name">`))
fmt.Fprint(w, data.UserName)
w.Write([]byte(`</span></p>
`))
}
w.Write([]byte(`

<form class="form" action="/keyword" method="POST">
  <ul>
    <li><input type="text" name="keyword"></li>
    <li><textarea name="description"></textarea></li>
    <li><input class="btn btn-primary" type="submit" value="Post" /></li>
  </ul>
</form>

`))
page := data.Page
w.Write([]byte(`

`))
for _, iter := range data.Entries {
_ = iter
w.Write([]byte(`
  `))
{
data := entry_with_ctx(*iter, data.UserName)
_ = data
w.Write([]byte(`<article>
  <h1><a href="/keyword/`))
fmt.Fprint(w, data.Entry.Keyword)
w.Write([]byte(`">`))
fmt.Fprint(w, data.Entry.Keyword)
w.Write([]byte(`</a></h1>
  <div>`))
fmt.Fprint(w, raw(data.Entry.Html))
w.Write([]byte(`</div>
  <button class="js-add-star" data-keyword="`))
fmt.Fprint(w, data.Entry.Keyword)
w.Write([]byte(`" data-user-name="`))
fmt.Fprint(w, data.UserName)
w.Write([]byte(`"><img src="`))
fmt.Fprint(w, url_for(`/img/star.gif`))
w.Write([]byte(`"></button>
  <span class="js-stars" data-keyword="`))
fmt.Fprint(w, data.Entry.Keyword)
w.Write([]byte(`">
    `))
for _, iter := range data.Entry.Stars {
_ = iter
w.Write([]byte(`<img src="`))
fmt.Fprint(w, url_for(`/img/star.gif`))
w.Write([]byte(`" title="`))
fmt.Fprint(w, iter.UserName)
w.Write([]byte(`" alt="`))
fmt.Fprint(w, iter.UserName)
w.Write([]byte(`">`))
}
w.Write([]byte(`</span>
</article>
`))
}
w.Write([]byte(`
`))
}
w.Write([]byte(`

<nav class="pagination">
  <ul>
`))
if truth, _ := template.IsTrue((page) > (1)); truth {
w.Write([]byte(`
  <li><a href="?page=`))
fmt.Fprint(w, sub(data.Page, 1))
w.Write([]byte(`">&laquo;</a></li>
`))
} else {
w.Write([]byte(`
  <li class="disabled"><span>&laquo;</span></li>
`))
}
w.Write([]byte(`
`))
for i, p := range data.Pages {
_ = i
_ = p
w.Write([]byte(`
  <li `))
if truth, _ := template.IsTrue((p) == (page)); truth {
w.Write([]byte(`class="active"`))
}
w.Write([]byte(`><a href="?page=`))
fmt.Fprint(w, p)
w.Write([]byte(`">`))
fmt.Fprint(w, p)
w.Write([]byte(`</a></li>
`))
}
w.Write([]byte(`
`))
if truth, _ := template.IsTrue((page) < (data.LastPage)); truth {
w.Write([]byte(`
  <li><a href="?page=`))
fmt.Fprint(w, add(page, 1))
w.Write([]byte(`">&raquo;</a></li>
`))
} else {
w.Write([]byte(`
  <li class="disabled"><span>&raquo;</span></li>
`))
}
w.Write([]byte(`
  </ul>
</nav>

`))
{
data := data
_ = data
w.Write([]byte(`    </div> <!-- /container -->

    <script type="text/javascript" src="`))
fmt.Fprint(w, url_for(`/js/jquery.min.js`))
w.Write([]byte(`"></script>
    <script type="text/javascript" src="`))
fmt.Fprint(w, url_for(`/js/bootstrap.min.js`))
w.Write([]byte(`"></script>
    <script type="text/javascript" src="`))
fmt.Fprint(w, url_for(`/js/star.js`))
w.Write([]byte(`"></script>
  </body>
</html>
`))
}
w.Write([]byte(`
`))
return nil}
