package main

import (
	"encoding/gob"
	"os"
	"strings"
	"time"
)

// Encode via Gob to file
func Save(path string, object interface{}) error {
	file, err := os.Create(path)
	if err == nil {
		encoder := gob.NewEncoder(file)
		encoder.Encode(object)
	}
	file.Close()
	return err
}

// Decode Gob file
func Load(path string, object interface{}) error {
	file, err := os.Open(path)
	if err == nil {
		decoder := gob.NewDecoder(file)
		err = decoder.Decode(object)
	}
	file.Close()
	return err
}

var baseUrl = "http://example.com/"

type Entry struct {
	ID          int
	AuthorID    int
	Keyword     string
	Description string
	UpdatedAt   time.Time
	CreatedAt   time.Time

	Html  string
	Stars []*Star
}

type User struct {
	ID        int
	Name      string
	Salt      string
	Password  string
	CreatedAt time.Time
}

type Star struct {
	ID        int       `json:"id"`
	Keyword   string    `json:"keyword"`
	UserName  string    `json:"user_name"`
	CreatedAt time.Time `json:"created_at"`
}

type EntryWithCtx struct {
	UserName string
	Entry    Entry
}

// for templates
func url_for(path string) string {
	return baseUrl + path
}
func title(s string) string {
	return strings.Title(s)
}
func raw(text string) string {
	return text
}
func add(a, b int) int { return a + b }
func sub(a, b int) int { return a - b }
func entry_with_ctx(entry Entry, ctx string) *EntryWithCtx {
	return &EntryWithCtx{UserName: ctx, Entry: entry}
}

type Data struct {
	UserName string
	Entries  []*Entry
	Page     int
	LastPage int
	Pages    []int
}
