package main
import (
	"io"
	"fmt"
	"text/template"
)
func PrecompiledTmpl_isucon_113(w io.Writer, data *Data) error {
{
data := data
_ = data
fmt.Fprint(w, `<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html" charset="utf-8">
    <title>Isuda</title>
    <link rel="shortcut icon" href="`)
fmt.Fprint(w, url_for(`favicon.ico`))
fmt.Fprint(w, `" type="image/vnd.microsoft.icon" />
    <link rel="stylesheet" href="`)
fmt.Fprint(w, url_for(`/css/bootstrap.min.css`))
fmt.Fprint(w, `">
    <link rel="stylesheet" href="`)
fmt.Fprint(w, url_for(`/css/bootstrap-responsive.min.css`))
fmt.Fprint(w, `">
    <link rel="stylesheet" href="`)
fmt.Fprint(w, url_for(`/css/main.css`))
fmt.Fprint(w, `">
  </head>
  <body>

    <div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <a class="brand" href="`)
fmt.Fprint(w, url_for(`/`))
fmt.Fprint(w, `">Isuda</a>
          <div class="nav-collapse">
            <ul class="nav">
              <li><a href="`)
fmt.Fprint(w, url_for(`/`))
fmt.Fprint(w, `">Home</a></li>
              <li><a href="`)
fmt.Fprint(w, url_for(`/login`))
fmt.Fprint(w, `">Login</a></li>
              <li><a href="`)
fmt.Fprint(w, url_for(`/register`))
fmt.Fprint(w, `">Register</a></li>
            </ul>
          </div> <!--/.nav-collapse -->
        </div>
      </div>
    </div>

    <div class="container">
      <p>Hello <span class="isu-account-name">`)
fmt.Fprint(w, data.UserName)
fmt.Fprint(w, `</span></p>
`)
}
fmt.Fprint(w, `

<form class="form" action="/keyword" method="POST">
  <ul>
    <li><input type="text" name="keyword"></li>
    <li><textarea name="description"></textarea></li>
    <li><input class="btn btn-primary" type="submit" value="Post" /></li>
  </ul>
</form>

`)
page := data.Page
fmt.Fprint(w, `

`)
for _, iter := range data.Entries {
_ = iter
fmt.Fprint(w, `
  `)
{
data := entry_with_ctx(*iter, data.UserName)
_ = data
fmt.Fprint(w, `<article>
  <h1><a href="/keyword/`)
fmt.Fprint(w, data.Entry.Keyword)
fmt.Fprint(w, `">`)
fmt.Fprint(w, data.Entry.Keyword)
fmt.Fprint(w, `</a></h1>
  <div>`)
fmt.Fprint(w, raw(data.Entry.Html))
fmt.Fprint(w, `</div>
  <button class="js-add-star" data-keyword="`)
fmt.Fprint(w, data.Entry.Keyword)
fmt.Fprint(w, `" data-user-name="`)
fmt.Fprint(w, data.UserName)
fmt.Fprint(w, `"><img src="`)
fmt.Fprint(w, url_for(`/img/star.gif`))
fmt.Fprint(w, `"></button>
  <span class="js-stars" data-keyword="`)
fmt.Fprint(w, data.Entry.Keyword)
fmt.Fprint(w, `">
    `)
for _, iter := range data.Entry.Stars {
_ = iter
fmt.Fprint(w, `<img src="`)
fmt.Fprint(w, url_for(`/img/star.gif`))
fmt.Fprint(w, `" title="`)
fmt.Fprint(w, iter.UserName)
fmt.Fprint(w, `" alt="`)
fmt.Fprint(w, iter.UserName)
fmt.Fprint(w, `">`)
}
fmt.Fprint(w, `</span>
</article>
`)
}
fmt.Fprint(w, `
`)
}
fmt.Fprint(w, `

<nav class="pagination">
  <ul>
`)
if truth, _ := template.IsTrue((page) > (1)); truth {
fmt.Fprint(w, `
  <li><a href="?page=`)
fmt.Fprint(w, sub(data.Page, 1))
fmt.Fprint(w, `">&laquo;</a></li>
`)
} else {
fmt.Fprint(w, `
  <li class="disabled"><span>&laquo;</span></li>
`)
}
fmt.Fprint(w, `
`)
for i, p := range data.Pages {
_ = i
_ = p
fmt.Fprint(w, `
  <li `)
if truth, _ := template.IsTrue((p) == (page)); truth {
fmt.Fprint(w, `class="active"`)
}
fmt.Fprint(w, `><a href="?page=`)
fmt.Fprint(w, p)
fmt.Fprint(w, `">`)
fmt.Fprint(w, p)
fmt.Fprint(w, `</a></li>
`)
}
fmt.Fprint(w, `
`)
if truth, _ := template.IsTrue((page) < (data.LastPage)); truth {
fmt.Fprint(w, `
  <li><a href="?page=`)
fmt.Fprint(w, add(page, 1))
fmt.Fprint(w, `">&raquo;</a></li>
`)
} else {
fmt.Fprint(w, `
  <li class="disabled"><span>&raquo;</span></li>
`)
}
fmt.Fprint(w, `
  </ul>
</nav>

`)
{
data := data
_ = data
fmt.Fprint(w, `    </div> <!-- /container -->

    <script type="text/javascript" src="`)
fmt.Fprint(w, url_for(`/js/jquery.min.js`))
fmt.Fprint(w, `"></script>
    <script type="text/javascript" src="`)
fmt.Fprint(w, url_for(`/js/bootstrap.min.js`))
fmt.Fprint(w, `"></script>
    <script type="text/javascript" src="`)
fmt.Fprint(w, url_for(`/js/star.js`))
fmt.Fprint(w, `"></script>
  </body>
</html>
`)
}
fmt.Fprint(w, `
`)
return nil}
