package main

import (
	"bytes"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
	"testing"

	"text/template"
)

// Compile all files under dir with matching extensions
func MakeTemplates_original(templates *template.Template, dir string, extensions []string) {
	filepath.Walk(dir, func(path string, info os.FileInfo, err error) error {
		if info == nil || info.IsDir() {
			return nil
		}
		rel, err := filepath.Rel(dir, path)
		if err != nil {
			return err
		}
		ext := ""
		if strings.Index(rel, ".") != -1 {
			ext = filepath.Ext(rel)
		}
		for _, extension := range extensions {
			if ext == extension {
				buf, err := ioutil.ReadFile(path)
				if err != nil {
					return err
				}
				name := filepath.ToSlash(rel[0 : len(rel)-len(ext)])
				tmpl := templates.New(name)

				template.Must(tmpl.Parse(string(buf)))
				break
			}
		}
		return err
	})
}

var data_original *Data = new(Data)
var templates_original = template.New("maintmpl")

func TestIsucon6q(t *testing.T) {
	buf1 := new(bytes.Buffer)
	buf2 := new(bytes.Buffer)
	if err := templates_original.ExecuteTemplate(buf1, "index", data_original); err != nil {
		panic(err)
	}
	PrecompiledTmpl_isucon_113(buf2, data_original)
	if buf1.String() != buf2.String() {
		panic("buf1 != buf2")
	}
}

func init() {
	err := Load("./isucon6q.gob", data_original)
	if err != nil {
		panic(err)
	}

	// make templates
	funcs := template.FuncMap{
		"url_for": func(path string) string {
			return baseUrl + path
		},
		"title": func(s string) string {
			return strings.Title(s)
		},
		"raw": func(text string) string {
			return text
		},
		"add": func(a, b int) int { return a + b },
		"sub": func(a, b int) int { return a - b },
		"entry_with_ctx": func(entry Entry, ctx string) *EntryWithCtx {
			return &EntryWithCtx{UserName: ctx, Entry: entry}
		},
	}
	templates_original = templates_original.Funcs(funcs)

	dir := "./views"
	extensions := []string{".tmpl"}

	MakeTemplates_original(templates_original, dir, extensions)
}

var NumLoop = 1000

func BenchmarkTextTemplate(b *testing.B) {
	for i := 0; i < NumLoop; i++ {
		buf := new(bytes.Buffer)
		err := templates_original.ExecuteTemplate(buf, "index", data_original)
		if err != nil {
			panic(err)
		}
	}
}

func BenchmarkPrecompiled(b *testing.B) {
	for i := 0; i < NumLoop; i++ {
		buf := new(bytes.Buffer)
		PrecompiledTmpl_isucon_113(buf, data_original)
	}
}

func BenchmarkPrecompiled2(b *testing.B) {
	for i := 0; i < NumLoop; i++ {
		buf := new(bytes.Buffer)
		PrecompiledTmpl_main_18(buf, data_original)
	}
}
