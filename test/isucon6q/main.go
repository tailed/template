package main

import (
	"bytes"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"

	"../.."
)

var data *Data = new(Data)
var templates = template.New("maintmpl")

func main() {
	buf := new(bytes.Buffer)
	if err := templates.ExecuteTemplate(buf, "index", data); err != nil {
		panic(err)
	}
}

func init() {
	err := Load("./isucon6q.gob", data)
	if err != nil {
		panic(err)
	}

	// make templates
	funcs := template.FuncMap{
		"url_for": func(path string) string {
			return baseUrl + path
		},
		"title": func(s string) string {
			return strings.Title(s)
		},
		"raw": func(text string) string {
			return text
		},
		"add": func(a, b int) int { return a + b },
		"sub": func(a, b int) int { return a - b },
		"entry_with_ctx": func(entry Entry, ctx string) *EntryWithCtx {
			return &EntryWithCtx{UserName: ctx, Entry: entry}
		},
	}
	templates = templates.Funcs(funcs)

	dir := "./views"
	extensions := []string{".tmpl"}

	MakeTemplates(templates, dir, extensions)
}

// Compile all files under dir with matching extensions
func MakeTemplates(templates *template.Template, dir string, extensions []string) {
	filepath.Walk(dir, func(path string, info os.FileInfo, err error) error {
		if info == nil || info.IsDir() {
			return nil
		}
		rel, err := filepath.Rel(dir, path)
		if err != nil {
			return err
		}
		ext := ""
		if strings.Index(rel, ".") != -1 {
			ext = filepath.Ext(rel)
		}
		for _, extension := range extensions {
			if ext == extension {
				buf, err := ioutil.ReadFile(path)
				if err != nil {
					return err
				}
				name := filepath.ToSlash(rel[0 : len(rel)-len(ext)])
				tmpl := templates.New(name)

				template.Must(tmpl.Parse(string(buf)))
				break
			}
		}
		return err
	})
}
