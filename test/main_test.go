package main

import (
	"text/template"
	"bytes"
	"testing"
	"os"
	"fmt"
)

func TestMain(t *testing.T) {
	fmap := template.FuncMap{
		"URLof": func (url string, person Person) string {
			return "http://example.com/" + url + "/" + person.UserName
		},
	}

	buf1 := new(bytes.Buffer)
	buf2 := new(bytes.Buffer)

	tmpl := generate_template()
	if err := template.Must(template.New("index.html").Funcs(fmap).Parse(tmpl)).Execute(buf1, Person{"taro"}); err != nil {
		panic(err)
	}
	PrecompiledTmpl_main_26_indexhtml(buf2, Person{"taro"}, fmap)
	if buf1.String() != buf2.String() {
		buf1.WriteTo(os.Stdout)
		fmt.Println("")
		fmt.Println("------")
		buf2.WriteTo(os.Stdout)
		panic("not equal")
	}
}
