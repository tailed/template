package main

import (
	"fmt"
	"io"
	"text/template"
)

func PrecompiledTmpl_main_27(w io.Writer, data *Data) error {
	w.Write([]byte(`<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Iscogram</title>
    <link href="/css/style.css" media="screen" rel="stylesheet" type="text/css">
  </head>
  <body>
    <div class="container">
      <div class="header">
        <div class="isu-title">
          <h1><a href="/">Iscogram</a></h1>
        </div>
        <div class="isu-header-menu">
          `))
	if truth, _ := template.IsTrue((data.Me.ID) == (0)); truth {
		w.Write([]byte(`
          <div><a href="/login">ログイン</a></div>
          `))
	} else {
		w.Write([]byte(`
          <div><a href="/@`))
		fmt.Fprint(w, data.Me.AccountName)
		w.Write([]byte(`"><span class="isu-account-name">`))
		fmt.Fprint(w, data.Me.AccountName)
		w.Write([]byte(`</span>さん</a></div>
          `))
		if truth, _ := template.IsTrue((data.Me.Authority) == (1)); truth {
			w.Write([]byte(`
          <div><a href="/admin/banned">管理者用ページ</a></div>
          `))
		}
		w.Write([]byte(`
          <div><a href="/logout">ログアウト</a></div>
          `))
	}
	w.Write([]byte(`
        </div>
      </div>

      `))
	{
		data := data
		_ = data
		w.Write([]byte(`
<div class="isu-submit">
  <form method="post" action="/" enctype="multipart/form-data">
    <div class="isu-form">
      <input type="file" name="file" value="file">
    </div>
    <div class="isu-form">
      <textarea name="body"></textarea>
    </div>
    <div class="form-submit">
      <input type="hidden" name="csrf_token" value="`))
		fmt.Fprint(w, data.CSRFToken)
		w.Write([]byte(`">
      <input type="submit" name="submit" value="submit">
    </div>
    `))
		if truth, _ := template.IsTrue(data.Flash); truth {
			w.Write([]byte(`
    <div id="notice-message" class="alert alert-danger">
      `))
			fmt.Fprint(w, data.Flash)
			w.Write([]byte(`
    </div>
    `))
		}
		w.Write([]byte(`
  </form>
</div>

`))
		{
			data := data.Posts
			_ = data
			w.Write([]byte(`<div class="isu-posts">
  `))
			for _, iter := range data {
				_ = iter
				w.Write([]byte(`
  `))
				{
					data := iter
					_ = data
					w.Write([]byte(`<div class="isu-post" id="pid_`))
					fmt.Fprint(w, data.ID)
					w.Write([]byte(`" data-created-at="`))
					fmt.Fprint(w, data.CreatedAt.Format(`2006-01-02T15:04:05-07:00`))
					w.Write([]byte(`">
  <div class="isu-post-header">
    <a href="/@`))
					fmt.Fprint(w, data.User.AccountName)
					w.Write([]byte(` " class="isu-post-account-name">`))
					fmt.Fprint(w, data.User.AccountName)
					w.Write([]byte(`</a>
    <a href="/posts/`))
					fmt.Fprint(w, data.ID)
					w.Write([]byte(`" class="isu-post-permalink">
      <time class="timeago" datetime="`))
					fmt.Fprint(w, data.CreatedAt.Format(`2006-01-02T15:04:05-07:00`))
					w.Write([]byte(`"></time>
    </a>
  </div>
  <div class="isu-post-image">
    <img src="`))
					fmt.Fprint(w, imageURL(data))
					w.Write([]byte(`" class="isu-image">
  </div>
  <div class="isu-post-text">
    <a href="/@`))
					fmt.Fprint(w, data.User.AccountName)
					w.Write([]byte(`" class="isu-post-account-name">`))
					fmt.Fprint(w, data.User.AccountName)
					w.Write([]byte(`</a>
    `))
					fmt.Fprint(w, data.Body)
					w.Write([]byte(`
  </div>
  <div class="isu-post-comment">
    <div class="isu-post-comment-count">
      comments: <b>`))
					fmt.Fprint(w, data.CommentCount)
					w.Write([]byte(`</b>
    </div>

    `))
					for _, iter := range data.Comments {
						_ = iter
						w.Write([]byte(`
    <div class="isu-comment">
      <a href="/@`))
						fmt.Fprint(w, iter.User.AccountName)
						w.Write([]byte(`" class="isu-comment-account-name">`))
						fmt.Fprint(w, iter.User.AccountName)
						w.Write([]byte(`</a>
      <span class="isu-comment-text">`))
						fmt.Fprint(w, iter.Comment)
						w.Write([]byte(`</span>
    </div>
    `))
					}
					w.Write([]byte(`
    <div class="isu-comment-form">
      <form method="post" action="/comment">
        <input type="text" name="comment">
        <input type="hidden" name="post_id" value="`))
					fmt.Fprint(w, data.ID)
					w.Write([]byte(`">
        <input type="hidden" name="csrf_token" value="`))
					fmt.Fprint(w, data.CSRFToken)
					w.Write([]byte(`">
        <input type="submit" name="submit" value="submit">
      </form>
    </div>
  </div>
</div>
`))
				}
				w.Write([]byte(`
  `))
			}
			w.Write([]byte(`
</div>
`))
		}
		w.Write([]byte(`

<div id="isu-post-more">
  <button id="isu-post-more-btn">もっと見る</button>
  <img class="isu-loading-icon" src="/img/ajax-loader.gif">
</div>
`))
	}
	w.Write([]byte(`
    </div>
    <script src="/js/jquery-2.2.0.js"></script>
    <script src="/js/jquery.timeago.js"></script>
    <script src="/js/jquery.timeago.ja.js"></script>
    <script src="/js/main.js"></script>
  </body>
</html>
`))
	return nil
}
