package main

import (
	"bytes"
	"testing"

	"text/template"
)

var data *Data = new(Data)
var templates = template.New("maintmpl")

func TestIsucon6q(t *testing.T) {
	buf1 := new(bytes.Buffer)
	buf2 := new(bytes.Buffer)
	if err := templates.Execute(buf1, data); err != nil {
		panic(err)
	}
	PrecompiledTmpl_main_27(buf2, data)
	if buf1.String() != buf2.String() {
		panic("buf1 != buf2")
	}
}

func init() {
	err := Load("./isucon-private.gob", data)
	if err != nil {
		panic(err)
	}

	fmap := template.FuncMap{
		"imageURL": imageURL,
	}

	templates = template.Must(template.New("layout.html").Funcs(fmap).ParseFiles(
		getTemplPath("layout.html"),
		getTemplPath("index.html"),
		getTemplPath("posts.html"),
		getTemplPath("post.html"),
	))
}

var NumLoop = 1000

func BenchmarkTextTemplate(b *testing.B) {
	for i := 0; i < NumLoop; i++ {
		buf := new(bytes.Buffer)
		err := templates.Execute(buf, data)
		if err != nil {
			panic(err)
		}
	}
}

func BenchmarkPrecompiled(b *testing.B) {
	for i := 0; i < NumLoop; i++ {
		buf := new(bytes.Buffer)
		PrecompiledTmpl_main_27(buf, data)
	}
}
