package main

import (
	"os"

	"../.."
)

func main() {
	var data *Data = new(Data)
	err := Load("./isucon-private.gob", data)
	if err != nil {
		panic(err)
	}

	fmap := template.FuncMap{
		"imageURL": imageURL,
	}

	buf := os.Stdout

	template.Must(template.New("layout.html").Funcs(fmap).ParseFiles(
		getTemplPath("layout.html"),
		getTemplPath("index.html"),
		getTemplPath("posts.html"),
		getTemplPath("post.html"),
	)).Execute(buf, data)
}
