package main

import (
	".."
	"os"
)

type Person struct {
	UserName string
}

func main() {
	fmap := template.FuncMap{
		"URLof": func (url string, person Person) string {
			return "http://example.com/" + url + "/" + person.UserName
		},
	}

	buf := os.Stdout

	tmpl := generate_template()



	for _, tmpl_name := range []string{"index.html", "layout.html"} {
		if err := template.Must(template.New(tmpl_name).Funcs(fmap).Parse(tmpl)).Execute(buf, Person{"taro"}); err != nil {
			panic(err)
		}
	}
}

func generate_template() string {
	return `
 {{ if not (.UserName) }} who are you?
 {{ else }} you have a name!
 {{ end }}

 {{ if . }} you are a person
 {{ else }} you are nil
 {{ end }}

 {{ "test<\"html>" | html }}


	hello {{ .UserName }}, go to {{ URLof "login" . }}
`
}
